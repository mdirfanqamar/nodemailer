module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        src: 'src/<%= pkg.name %>.js',
        dest: 'build/<%= pkg.name %>.min.js'
      }
    }
	
	nodemailer: {

    options: {
      transport: {
        type: 'SMTP',
        options: {
          service: 'Gmail',
          auth: {
            user: 'mdirfanqamar@gmail.com',
            pass: 'password'
          }
        }
      },
      message: {
        subject: 'Node Mailer task',
        text: 'TOCI',
        html: '<body><h1>HTML custom message</h1></body>',
      },
      recipients: [
        {
          email: 'mohammadhammad18@hotmail.com',
          name: 'Hammad Shabbir'
        }
      ]
    },

    inline: { /* use above options*/ },

    external: {
      src: ['check.txt']
    }
  }
	
	
	
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-nodemailer');

  // Default task(s).
  //grunt.registerTask('default', ['uglify']);

};